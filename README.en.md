# RXThinkCMF_ThinkPhp5.1_AntdVue

#### Description
RXThinkCMF_ThinkPhp5.1_AntdVue 旗舰版是一款基于 ThinkPhp5.1 + Vue + ElementUI研发的前后端分离框架，随着系统框架的精细化发展，越来越多的项目采用前后端分离的架构，因此我们重点打造了一款前后端分离架构的PHP框架，为了简化开发，框架集成了完整的权限架构体系以及常规基础模块如：用户管理、角色管理、菜单管理、职级管理、岗位管理、日志管理以及广告管理、页面布局管理、字典管理、配置管理等等，前端UI支持多主题切换，可以设置自己喜欢的样式风格，同时框架集成了代码生成器可以一键生成CURD所有模块文件及代码(同时会自动生成前端UI代码)，以便开发者快速构建自己的应用。专注于为中小企业提供最佳的行业基础后台框架解决方案，提高执行效率、扩展性、稳定性值得信赖，操作体验流畅，欢迎大家使用！！

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

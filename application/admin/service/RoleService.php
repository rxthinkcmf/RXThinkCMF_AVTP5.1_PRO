<?php
// +----------------------------------------------------------------------
// | RXThinkCMF敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace app\admin\service;

use app\admin\model\Menu;
use app\admin\model\Role;
use app\admin\model\RoleMenu;

/**
 * 角色管理-服务类
 * @author 牧羊人
 * @since 2020/11/20
 * Class RoleService
 * @package app\admin\service
 */
class RoleService extends BaseService
{
    /**
     *
     * @author 牧羊人
     * @since 2020/11/20
     * RoleService constructor.
     */
    public function __construct()
    {
        $this->model = new Role();
    }

    /**
     * 获取数据列表
     * @return array
     * @since 2020/11/20
     * @author 牧羊人
     */
    public function getList()
    {
        return parent::getList([], "id asc"); // TODO: Change the autogenerated stub
    }

    /**
     * 获取角色列表
     * @return array
     * @since 2020/11/20
     * @author 牧羊人
     */
    public function getRoleList()
    {
        $list = $this->model->where([
            ['status', '=', 1],
            ['mark', '=', 1],
        ])->order("sort", "asc")
            ->select()
            ->toArray();
        return message("操作成功", true, $list);
    }

    /**
     * 获取权限列表
     * @return array
     * @since 2020/11/20
     * @author 牧羊人
     */
    public function getPermissionList()
    {
        // 请求参数
        $param = request()->param();
        // 角色ID
        $roleId = intval(getter($param, "role_id", 0));
        // 获取全部菜单
        $menuModel = new Menu();
        $menuList = $menuModel->where([
            ['status', '=', 1],
            ['mark', '=', 1],
        ])->order("sort", "asc")->select()->toArray();
        if (!empty($menuList)) {
            $roleMenuModel = new RoleMenu();
            $roleMenuList = $roleMenuModel
                ->field("menu_id")
                ->where("role_id", '=', $roleId)
                ->select()
                ->toArray();
            $menuIdList = array_key_value($roleMenuList, "menu_id");
            foreach ($menuList as &$val) {
                if (in_array($val['id'], $menuIdList)) {
                    $val['checked'] = true;
                    $val['open'] = true;
                }
            }
        }
        return message("操作成功", true, $menuList);
    }

    /**
     * 保存权限
     * @return array
     * @throws \Exception
     * @author 牧羊人
     * @since 2020/11/20
     */
    public function savePermission()
    {
        // 请求参数
        $param = request()->param();
        // 角色ID
        $roleId = intval(getter($param, "role_id", 0));
        if (!$roleId) {
            return message("角色ID不能为空", false);
        }

        // 删除角色菜单关系数据
        $roleMenuModel = new RoleMenu();
        $roleMenuModel->where("role_id", $roleId)->delete();

        // 插入角色菜单关系数据
        $menuIds = isset($param["menu_id"]) ? $param["menu_id"] : [];
        $list = [];
        if (is_array($menuIds) && !empty($menuIds)) {
            foreach ($menuIds as $val) {
                $data = [
                    'role_id' => $roleId,
                    'menu_id' => $val,
                ];
                $list[] = $data;
            }
        }
        $roleMenuModel->insertAll($list);
        return message();
    }

}
<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]
namespace think;

// 加载基础文件
require __DIR__ . '/../thinkphp/base.php';

// 支持事先使用静态方法设置Request对象和Config对象

switch ($_SERVER['HTTP_HOST']) {
    case 'www.thinkphp5.1.antdvue':
        // 网站模块
        $model = 'index';
        break;
    case 'admin.thinkphp5.1.antdvue':
        // admin模块
        $model = 'admin';
        break;
    case 'm.thinkphp5.1.antdvue':
        // 手机站模块
        $model = 'm';
        break;
    case 'api.thinkphp5.1.antdvue':
        // API接口模块
        $model = 'api';
        break;
    case 'script.thinkphp5.1.antdvue':
        // 脚本模块
        $model = 'script';
        break;
}

// 执行应用并响应
Container::get('app')->bind($model)->run()->send();
